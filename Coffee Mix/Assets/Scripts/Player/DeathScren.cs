using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathScren : MonoBehaviour
{
    public GameObject ds1; //vida
    public GameObject ds2; //boom
    public GameObject ds3; //frio
    public static bool quit = false;

    void Start()
    {
        ds1.SetActive(false);
        ds2.SetActive(false);
        ds3.SetActive(false);
    }

    void Update()
    {
        quitGame();
        if (HUD.lifeCounter == 0)
        {
            StartCoroutine(DS2());
        }

        if (HUD.mistakeCounter == 0)
        {
            StartCoroutine(DS1());
        }
    }

    IEnumerator DS1()
    {
        yield return new WaitForSeconds(5f);
        ds1.SetActive(true);
        ControlJugador.stop = true;
        GestorDeAudio.instancia.PausarSonido("MainMusic");
        quit = true;
    }

    IEnumerator DS2()
    {
        yield return new WaitForSeconds(7f);
        ds2.SetActive(true);
        ControlJugador.stop = true;
        GestorDeAudio.instancia.PausarSonido("MainMusic");
        quit = true;
    }

    private void quitGame()
    {
        if (quit == true && Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

   

}
