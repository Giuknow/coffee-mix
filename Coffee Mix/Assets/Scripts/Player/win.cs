using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class win : MonoBehaviour
{
    public static int wincount = 0;
    public GameObject winScreen;

    void Start()
    {
        winScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (wincount == 4)
        {
            winScreen.SetActive(true);
            ControlJugador.stop = true;
            GestorDeAudio.instancia.PausarSonido("MainMusic");
            DeathScren.quit = true;
        }
    }
}
