using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUD : MonoBehaviour
{
    public GameObject Heart1;
    public GameObject Heart2;
    public GameObject Heart3;


    public GameObject Hud;
    public Animator Camera;
    public bool activar = true; //False para evitar animacion 
    public static int lifeCounter;
    public static int mistakeCounter;

    void Start()
    {
        lifeCounter = 3;
        mistakeCounter = 1;

        if (activar)
        {
            Hud.SetActive(false);
            Camera.Play("CameraStart");
            ControlJugador.stop = true;
            StartCoroutine(Wait4HUD());
        }
    }


    void Update()
    {
        if (lifeCounter == 3)
        {
            Heart3.SetActive(true);
            Heart2.SetActive(true);
            Heart1.SetActive(true);
        }
        else if (lifeCounter == 2)
        {
            Heart3.SetActive(false);
            Heart2.SetActive(true);
            Heart1.SetActive(true);
        }
        else if (lifeCounter == 1)
        {
            Heart3.SetActive(false);
            Heart2.SetActive(false);
            Heart1.SetActive(true);
        }
        else if (lifeCounter == 0)
        {
            Heart3.SetActive(false);
            Heart2.SetActive(false);
            Heart1.SetActive(false);
            ControlJugador.stop = true;
        }

      
    }

    IEnumerator Wait4HUD()
    {
        yield return new WaitForSeconds(30f);
        Hud.SetActive(true);
    }
}

