using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chili : MonoBehaviour
{
    public GameObject puerta;
    public GameObject HotState;
    public Animator tirarPuerta;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Puerta") == true)
        {
            if (Mix.aji == true)
            {
                puerta.SetActive(false);
                Mix.aji = false;
                ControlJugador.rapidez = 3f;
                GestorDeAudio.instancia.ReproducirSonido("DoorBreaking");
                tirarPuerta.Play("Puerta");
                HotState.SetActive(false);
            }
            
        }
    }

        void Start()
    {
        
    }

  
    void Update()
    {
        
    }
}
