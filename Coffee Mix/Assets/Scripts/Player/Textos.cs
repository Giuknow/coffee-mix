using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Textos : MonoBehaviour
{
    public GameObject textoUno;
    public GameObject textoDos;
    public GameObject textoTres;
    public GameObject textoCuatro;
    public GameObject textoCinco;
    public GameObject textoSeis;
    public GameObject HUD;

    public Animator camera;
    public bool skip = false;
    public bool control = false;

    void Start()
    {
        StartCoroutine(Texto2());
        textoUno.SetActive(false);
        textoTres.SetActive(false);
        textoCuatro.SetActive(false);
        textoCinco.SetActive(false);
        textoSeis.SetActive(false);
        HUD.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && skip == false)
        {
            skip = true;
            SkipAnimation();
            StartCoroutine(HUDSkip());
        }

        if (skip)
        {
            textoDos.SetActive(false);
            textoUno.SetActive(false);
            textoTres.SetActive(false);
            textoCuatro.SetActive(false);
            textoCinco.SetActive(false);
            textoSeis.SetActive(false);
        }
    }

    IEnumerator Texto2()
    {
        yield return new WaitForSeconds(5f);
        textoDos.SetActive(false);
        textoUno.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto3());
        }
    }

    IEnumerator Texto3()
    {
        yield return new WaitForSeconds(5f);
        textoUno.SetActive(false);
        textoTres.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto4());
        }
    }

    IEnumerator Texto4()
    {
        yield return new WaitForSeconds(5f);
        textoTres.SetActive(false);
        textoCuatro.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto5());
        }
    }

    IEnumerator Texto5()
    {
        yield return new WaitForSeconds(5f);
        textoCuatro.SetActive(false);
        textoCinco.SetActive(true);
        if (skip == false)
        {
            StartCoroutine(Texto6());
        }
    }

    IEnumerator Texto6()
    {
        yield return new WaitForSeconds(5f);
        textoCinco.SetActive(false);
        textoSeis.SetActive(true);
        StartCoroutine(Finalizar());
        ControlJugador.stop = false;
    }
    IEnumerator Finalizar()
    {
        yield return new WaitForSeconds(5f);
        
        textoSeis.SetActive(false);
        
    }

    private void SkipAnimation()
    {
        if (skip == true && control == false)
        {
            control = true;
            camera.Play("Skip");
            ControlJugador.stop = false;
        }
    }

    IEnumerator HUDSkip()
    {
        yield return new WaitForSeconds(1f);
        HUD.SetActive(true);
    }

}
