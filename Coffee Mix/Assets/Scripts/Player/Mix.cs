using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mix : MonoBehaviour
{
    public int indice;
    public static bool iniciar = false;
    public static bool beber = false;
    public bool cafeListo = false;
    public static bool aji = false;

    public GameObject trash;
    public GameObject chili;
    public GameObject toxic;
    public GameObject milk;
    public GameObject ice;

    public GameObject estado1; //veneno
    public GameObject estado2; //Calor

    public GameObject taza;
    public GameObject cafe;
    public GameObject cafeToxico;
    public GameObject cafeConLeche;
    public GameObject cafePicante;
    public GameObject cafeFrio;

    public bool poison = false;

    public ParticleSystem humo;

    void Start()
    {
        cafe.SetActive(false);
        cafeConLeche.SetActive(false);
        cafePicante.SetActive(false);
        cafeFrio.SetActive(false);
        cafeToxico.SetActive(false);
        trash.SetActive(false);
        humo.Stop();

        estado1.SetActive(false);
        estado2.SetActive(false);
    }

    void Update()
    {
        indice = PlayerCoffee.ingrediente;
        mixer();
        tomarCafe();
    }

    public void mixer()
    {
        if (indice == 10 && iniciar==true && cafeListo == false)
        {
            cafe.SetActive(true);
            cafeListo = true;
            taza.SetActive(false);
        }

        if (indice == 1 && iniciar == true && cafeListo == false)
        {
            milk.SetActive(false);
            cafeConLeche.SetActive(true);
            cafeListo = true;
            taza.SetActive(false);
        }

        if (indice == 4 && iniciar == true && cafeListo == false)
        {
            chili.SetActive(false);
            aji = true;
            cafePicante.SetActive(true);
            cafeListo = true;
            taza.SetActive(false);
        }

        if (indice == 3 && iniciar == true && cafeListo == false)
        {
            toxic.SetActive(false);
            cafeToxico.SetActive(true);
            cafeListo = true;
            taza.SetActive(false);
        }
        if (indice == 5 && iniciar == true && cafeListo == false)
        {
            ice.SetActive(false);
            cafeFrio.SetActive(true);
            cafeListo = true;
            taza.SetActive(false);
        }
        if (indice == 2 && iniciar == true && cafeListo == false)
        {
            GestorDeAudio.instancia.ReproducirSonido("Explode");
            cafeListo = true;
            humo.Play();
            HUD.mistakeCounter -= 1;
        }
        if (indice == 7 && iniciar == true && cafeListo == false)
        {
            GestorDeAudio.instancia.ReproducirSonido("Explode");
            cafeListo = true;
            humo.Play();
            HUD.mistakeCounter -= 1;
        }
    }
    public void tomarCafe()
    {
        if(cafeListo==true && indice == 1 && Input.GetKeyDown(KeyCode.E))
        {
            GestorDeAudio.instancia.ReproducirSonido("drink");
            cafeConLeche.SetActive(false);
            cafeListo = false;
            poison = false;
            ControlJugador.rapidez = 3f;
            estado1.SetActive(false);
            estado2.SetActive(false);
            trash.SetActive(true);
            PlayerCoffee.ingrediente=0;
            win.wincount++;
        }

        if (cafeListo == true && indice == 3 && Input.GetKeyDown(KeyCode.E))
        {
            GestorDeAudio.instancia.ReproducirSonido("drink");
            cafeToxico.SetActive(false);
            cafeListo = false;
            PoisonEffect();
            estado1.SetActive(true);
            estado2.SetActive(false);
            trash.SetActive(true);
            PlayerCoffee.ingrediente = 0;
            win.wincount++;
        }

        if (cafeListo == true && indice == 4 && Input.GetKeyDown(KeyCode.E) && aji == true && CafeteraCol.animation == true)
        {
            GestorDeAudio.instancia.ReproducirSonido("ouch");
            HUD.lifeCounter -= 1;
            taza.SetActive(false);
            cafeListo = false;
            cafePicante.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("ChooChoo");
            ControlJugador.rapidez += 4f;
            estado1.SetActive(false);
            estado2.SetActive(true);
            trash.SetActive(true);
            PlayerCoffee.ingrediente = 0;
            win.wincount++;
        }

        if (cafeListo == true && indice == 5 && Input.GetKeyDown(KeyCode.E))
        {
            GestorDeAudio.instancia.ReproducirSonido("drink");
            cafeListo = false;
            cafeFrio.SetActive(false);
            trash.SetActive(true);
            PlayerCoffee.ingrediente = 0;
            win.wincount++;
        }

        if (cafeListo == true && indice == 10 && Input.GetKeyDown(KeyCode.E))
        {
            GestorDeAudio.instancia.ReproducirSonido("drink");
            cafeListo = false;
            cafe.SetActive(false);
            if (HUD.lifeCounter < 3)
            {
                HUD.lifeCounter += 1;
            }
            PlayerCoffee.ingrediente = 0;
        }

    }

    private void PoisonEffect()
    {
        poison = true;
        StartCoroutine(PoisonCount());
    }

    IEnumerator PoisonCount()
    {

        yield return new WaitForSeconds(7f);
        if (poison)
        {
            GestorDeAudio.instancia.ReproducirSonido("ouch");
            HUD.lifeCounter -= 1;
            StartCoroutine(PoisonCount());
        }
    }

}

