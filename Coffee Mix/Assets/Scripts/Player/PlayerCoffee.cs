using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCoffee : MonoBehaviour
{
    public GameObject taza;
    public GameObject t;

    public GameObject tazaLeche;
    public GameObject tazaNafta;
    public GameObject tazaToxico;
    public GameObject tazaPimiento;
    public GameObject tazahielo;

    public GameObject ice;
    public GameObject fuel; 
    public GameObject flask; 
    public GameObject milk;
    public GameObject pepper;

    public static bool tazacontrol = true;
    public bool accion = false;
    public static bool permitir = false;
    public static int ingrediente=0;
    public bool ponerIngrediente = false;
    public bool tazaPuesta = false;

    public void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.CompareTag("Cafetera")==true && itemCol.hayTaza==true)
        {
          accion = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
       if(other.gameObject.CompareTag("Cafetera") == true && itemCol.hayTaza == true)
        {
            accion=false;
        }
    }
    void Start()
    {
        t.SetActive(false);
        tazaLeche.SetActive(false);
        tazaNafta.SetActive(false);
        tazaToxico.SetActive(false);
        tazaPimiento.SetActive(false);
        tazahielo.SetActive(false);
    }

   
    void Update()
    {
        ponerIngredientes();
        if (Input.GetKeyDown(KeyCode.E) && itemCol.hayTaza == true && accion==true && CafeteraCol.ponerTaza == true && tazacontrol == false)
        {
            tazacontrol = true;
            itemCol.itemGrabbed = false;
            t.SetActive(true);
            itemCol.hayTaza = false;
            taza.SetActive(false);
            CafeteraCol.ponerTaza = false;
            ponerIngrediente = true;
            tazaPuesta = true;
        }

    }

    public void ingredientes()
    {
        
    }

    private void ponerIngredientes()
    {
        if(Input.GetKeyDown(KeyCode.E) && ingrediente == 1 && permitir==true && tazaPuesta) //leche
        {
            GestorDeAudio.instancia.ReproducirSonido("Liquid");
            tazaLeche.SetActive(true);
            t.SetActive(false);
            permitir = false;
            t.SetActive(false);
            milk.SetActive(false);
           
        }
        if (Input.GetKeyDown(KeyCode.E) && ingrediente == 2 && permitir == true && tazaPuesta) //nafta
        {
            GestorDeAudio.instancia.ReproducirSonido("Liquid");
            tazaNafta.SetActive(true);
            t.SetActive(false);
            permitir = false;
            fuel.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.E) && ingrediente == 3 && permitir == true && tazaPuesta) //frasco
        {
            GestorDeAudio.instancia.ReproducirSonido("Acid");
            tazaToxico.SetActive(true);
            t.SetActive(false);
            permitir = false;
            flask.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.E) && ingrediente == 4 && permitir == true && tazaPuesta) //pimiento
        {

            tazaPimiento.SetActive(true);
            t.SetActive(false);
            permitir = false;
            pepper.SetActive(false);

        }
        if (Input.GetKeyDown(KeyCode.E) && ingrediente == 5 && permitir == true && tazaPuesta) //hielo
        {
            GestorDeAudio.instancia.ReproducirSonido("Ice");
            tazahielo.SetActive(true);
            t.SetActive(false);
            permitir = false;
            ice.SetActive(false);

        }


    }
}
