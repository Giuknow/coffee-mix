using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemCol : MonoBehaviour
{
    public Animator Bin1;
    public Animator Bin2;

    public GameObject taza;
    public GameObject nafta;
    public GameObject frasco;
    public GameObject leche;
    public GameObject pepper;
    public GameObject Bin;
    public GameObject iceJugador;

    public GameObject trash;
    public GameObject lecheJugador;
    public GameObject naftaJugador;
    public GameObject frascoJugador;
    public GameObject tazaJugador;
    public GameObject pepperJugador;

    public GameObject E;

    public bool l = false;
    public static bool t = false;
    public bool n = false;
    public bool f = false;
    public bool p = false;
    public static bool hayTaza = false;

    public bool binState = false;
    public static bool itemGrabbed = false;


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("leche") == true)
        {
            l = true;
            E.SetActive(true);
        }
        if (other.gameObject.CompareTag("nafta") == true)
        {
            n = true;
            E.SetActive(true);
        }
        if (other.gameObject.CompareTag("taza") == true)
        {
            t = true;
            E.SetActive(true);
        }
        if (other.gameObject.CompareTag("frasco") == true)
        {
            f = true;
            E.SetActive(true);
        }
        if (other.gameObject.CompareTag("pepper") == true)
        {
            p = true;
            E.SetActive(true);
        }
        if (other.gameObject.CompareTag("bin") == true)
        {
            binState = true;
            E.SetActive(true);
        }

    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("leche") == true)
        {
            l = false;
            E.SetActive(false);
        }
        if (other.gameObject.CompareTag("nafta" ) == true || other.gameObject.CompareTag("TNT")== true)
        {
            n = false;
            E.SetActive(false);

        }
        if (other.gameObject.CompareTag("taza") == true)
        {
            t = false;
            E.SetActive(false);
        }
        if (other.gameObject.CompareTag("frasco") == true)
        {
            f = false;
            E.SetActive(false);
        }
        if (other.gameObject.CompareTag("pepper") == true)
        {
            p = false;
            E.SetActive(false);
        }
        if (other.gameObject.CompareTag("bin") == true)
        {
            binState = false;
            E.SetActive(false);
        }
    }


    void Start()
    {
        lecheJugador.SetActive(false);
        naftaJugador.SetActive(false);
        tazaJugador.SetActive(false);
        frascoJugador.SetActive(false);
        pepperJugador.SetActive(false);
        E.SetActive(false);
        trash.SetActive(false);

    } 

   
    void Update() 
    { 
     if(Input.GetKeyDown(KeyCode.E) && l == true && itemGrabbed == false)
        {
            l = false;
            GestorDeAudio.instancia.ReproducirSonido("Grab");
            leche.SetActive(false);
            lecheJugador.SetActive(true);
            itemGrabbed = true;
            PlayerCoffee.ingrediente = 1;
            E.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.E) && n == true && itemGrabbed == false)
        {
            n = false;
            GestorDeAudio.instancia.ReproducirSonido("Grab");
            nafta.SetActive(false);
            naftaJugador.SetActive(true);
            itemGrabbed = true;
            PlayerCoffee.ingrediente = 2;
            E.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.E) && t == true && itemGrabbed == false)
        {
            PlayerCoffee.tazacontrol = false;
            itemGrabbed = true;
            t = false;
            GestorDeAudio.instancia.ReproducirSonido("Grab");
            taza.SetActive(false);
            hayTaza = true;
            tazaJugador.SetActive(true);
            PlayerCoffee.ingrediente = 10;
            E.SetActive(false);

        }

        if (Input.GetKeyDown(KeyCode.E) && f == true && itemGrabbed == false)
        {
            f = false;
            GestorDeAudio.instancia.ReproducirSonido("Grab");
            frasco.SetActive(false);
            frascoJugador.SetActive(true);
            itemGrabbed = true;
            PlayerCoffee.ingrediente = 3;
            E.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.E) && p == true && itemGrabbed == false)
        {
            p = false;
            GestorDeAudio.instancia.ReproducirSonido("Grab");
            pepper.SetActive(false);
            pepperJugador.SetActive(true);
            itemGrabbed = true;
            PlayerCoffee.ingrediente = 4;
            E.SetActive(false);
        }
        //TACHO DE BASURA
        if (binState == true && Input.GetKeyDown(KeyCode.E))
        {
            if (itemGrabbed)
            {
                itemGrabbed = false;
                tazaJugador.SetActive(false); 
                lecheJugador.SetActive(false);
                naftaJugador.SetActive(false);
                frascoJugador.SetActive(false);
                pepperJugador.SetActive(false);
                iceJugador.SetActive(false);
                trash.SetActive(false);
                //Animacion y Sonido
                GestorDeAudio.instancia.ReproducirSonido("Bin");
                Bin1.Play("Bin1");
                StartCoroutine(Play());

            }
        }
    }

    IEnumerator Play()
    {
        yield return new WaitForSeconds(0.15f);
        Bin2.Play("Bin2f");

    }
}
