using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FridgeCol : MonoBehaviour
{
    public Animator fridgeDoorOpen;
    public bool onFridge = false;
    public bool fridgeIsOpen = false;
    public float rapidez;
    public ParticleSystem frio;
    public bool done = false;
    public GameObject hielo;
    public GameObject iceCube3;
    public GameObject iceCube2;
    public GameObject iceCube1;
    public bool cubes = true;
    public GameObject Estado1;
    public GameObject Estado2;
    public GameObject Estado3;
    public GameObject ds3;

    public GameObject E;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Fridge") == true)
        {
            onFridge = true;
            E.SetActive(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Fridge") == true)
        {
            onFridge = false;
            E.SetActive(false);
        }
    }

    void Start()
    {
        Estado3.SetActive(false);
        frio.Stop();
        hielo.SetActive(false);
        iceCube1.SetActive(false);
        iceCube2.SetActive(false);
        iceCube3.SetActive(false);
        E.SetActive(false);
        ds3.SetActive(false);
    }


    void Update()
    {
        if (onFridge == true && Input.GetKeyDown(KeyCode.E) && done == false)
        {
            fridgeDoorOpen.Play("Fridge");
            frio.Play();
            GestorDeAudio.instancia.ReproducirSonido("Snow");
            PlayerCoffee.ingrediente = 5;
            itemCol.itemGrabbed = true;
            done = true;
            hielo.SetActive(true);
            E.SetActive(false);
            fridgeIsOpen = true;
        }

        if (fridgeIsOpen)
        {
            StartCoroutine(Freeze());
            Estado1.SetActive(false);
            Estado2.SetActive(false);
            Estado3.SetActive(true);
            if (cubes == true)
            {
                cubes = false;
                StartCoroutine(Ice1());
                StartCoroutine(Ice2());
                StartCoroutine(Ice3());
            }
        }

        if (rapidez < 0)
        {
            fridgeIsOpen = false;
            StartCoroutine(DS3());
            ControlJugador.stop = true;
        }
    }

    IEnumerator Freeze()
    {
        if (fridgeIsOpen == true)
        {
            yield return new WaitForSeconds(0.5f);
            ControlJugador.rapidez -= 0.0001f;
            rapidez = ControlJugador.rapidez;
        }
    }
    IEnumerator Ice3()
    {
        yield return new WaitForSeconds(35f);
        iceCube2.SetActive(false);
        iceCube3.SetActive(true);
        ControlJugador.stop = true;
    }
    IEnumerator Ice1()
    {

        yield return new WaitForSeconds(10f);
        iceCube1.SetActive(true);
        ControlJugador.rapidez -= 1;
    }
    IEnumerator Ice2()
    {
        yield return new WaitForSeconds(20f);
        iceCube1.SetActive(false);
        iceCube2.SetActive(true);
        ControlJugador.rapidez -= 1f;
    }
    IEnumerator DS3()
    {
        yield return new WaitForSeconds(5f);
        ds3.SetActive(true);
        GestorDeAudio.instancia.PausarSonido("MainMusic");
        DeathScren.quit = true;
    }
}
