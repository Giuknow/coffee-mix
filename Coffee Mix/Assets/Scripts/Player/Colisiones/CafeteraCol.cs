using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CafeteraCol : MonoBehaviour
{
    public GameObject ON;
    public GameObject OFF;
    public GameObject boton;
    public GameObject HUD;

    public GameObject E;

    public bool escape = false;
    public Animator cameraMov_1;
    public Animator cameraMov_2;
    public static bool animation = false;
    public bool control = false;
    public static bool ponerTaza = false;
    public bool C = false;

     public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cafetera") == true)
        {
            C = true;
            control = true;
            E.SetActive(true);
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Cafetera") == true)
        {
            C = false;
            if (C == false)
            {
                E.SetActive(false);
            }
           
            
        }
    }

    void Update()
    {
        if (animation && Input.GetKeyDown(KeyCode.Escape) && escape)
        {
            ControlJugador.stop = false;
            ControlJugador.stop = false;
            animation = false;
            cameraMov_2.Play("CameraMov2");
            ponerTaza = false;
            PlayerCoffee.permitir = false;
            BotonesNO();
            StartCoroutine(Wait4HUD());

        }
        else if (Input.GetKeyDown(KeyCode.E) && control && C == true)
        {
            ControlJugador.stop = true;
            HUD.SetActive(false);
            ControlJugador.stop = true;
            control = false;
            animation = true;
            cameraMov_1.Play("CameraMov1");
            E.SetActive(false);
            StartCoroutine(KeyE());           
        }

        IEnumerator KeyE()
        {
            yield return new WaitForSeconds(1f);
            ponerTaza = true;
            PlayerCoffee.permitir = true;
            BotonesSI(); 
        }

    }

    private void BotonesSI()
    {
        escape = true;
        boton.SetActive(true);
    }
    private void BotonesNO()
    {
        escape = false;
        boton.SetActive(false);
        
    }

    IEnumerator Wait4HUD()
    {
        yield return new WaitForSeconds(1f);
        HUD.SetActive(true);
    }
}
