using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechoCol : MonoBehaviour
{
    public GameObject techoA;
    public GameObject techoB;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Techo") == true)
        {
            other.gameObject.SetActive(false);
            GestorDeAudio.instancia.ReproducirSonido("pop");
        }
    }

}
