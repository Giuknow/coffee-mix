using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador2 : MonoBehaviour
{
    private Rigidbody rb;
    public int rapidez;
    public static bool stop = false;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        if (stop == false)
        {
            float movimientoVertical = Input.GetAxis("Vertical");
            float movimientoHorizontal = Input.GetAxis("Horizontal");
            Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical);
            rb.AddForce(vectorMovimiento * rapidez);
            if (vectorMovimiento != Vector3.zero)
            {
                transform.forward = vectorMovimiento;
            }
        }
    }
}